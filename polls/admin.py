from django.contrib import admin
from .models import Choice, Question, VotedOn


admin.site.empty_value_display = '(Empty Value)'

@admin.register(Choice)
class MyChoice(admin.ModelAdmin):
    list_display = ('choice_text', 'pk', 'question', 'votes', 'user_id',)
    ordering = ('choice_text', 'votes',)
    readonly_fields = ('user_id', 'pk', 'votes',)


@admin.register(Question)
class MyQuestion(admin.ModelAdmin):
    list_display = ('question_text', 'pk', 'pub_date', 'user_id',)
    ordering = ('question_text', 'pub_date',)
    readonly_fields = ('user_id', 'pk',)


@admin.register(VotedOn)
class MyChoice(admin.ModelAdmin):
    list_display = ('question', 'user_id',)
    ordering = ('user_id', 'question',)
    readonly_fields = ('question', 'user_id',)

