from rest_framework import generics, mixins
from polls.models import Question, Choice
from .serializers import QuestionSerializer, ChoiceSerializer
from rest_framework.response import Response
from rest_framework import permissions, status, viewsets, views
from django.shortcuts import get_object_or_404, render, redirect, Http404


class QuestionAPI(viewsets.ViewSet):
    def get_object(self, pk):
        try:
            return Question.objects.get(pk=pk)
        except Question.DoesNotExist:
            raise Http404

    def list(self, request):
        queryset = Question.objects.all()
        serializer = QuestionSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = QuestionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object(kwargs['pk'])
        serializer = QuestionSerializer(instance=instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = self.get_object(kwargs['pk'])
        serializer = QuestionSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.update(instance, request.data)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        instance = Question.objects.get(pk=kwargs['pk'])
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ChoiceAPI(viewsets.ViewSet):
    def get_object(self, pk):
        try:
            return Choice.objects.get(pk=pk)
        except Choice.DoesNotExist:
            raise Http404

    def list(self, request):
        queryset = Choice.objects.all()
        serializer = ChoiceSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = ChoiceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object(kwargs['pk'])
        serializer = ChoiceSerializer(instance=instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = self.get_object(kwargs['pk'])
        serializer = ChoiceSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.update(instance, request.data)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        instance = Choice.objects.get(pk=kwargs['pk'])
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


