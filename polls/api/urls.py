from .views import QuestionAPI, ChoiceAPI
from django.conf.urls import url
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'questions', QuestionAPI, basename='user')
router.register(r'choice', ChoiceAPI, basename='user')
urlpatterns = router.urls

'''
urlpatterns = [
    #url(r'^question/(?P<pk>\d+)/$', QuestionAPI.as_view(), name='question_api'),
    url(r'^question/$', QuestionAPI.as_view({'get': 'list'}), name='question_api_add'),
    url(r'^choice/(?P<pk>\d+)/$', ChoiceAPI.as_view(), name='choice_api'),
    url(r'^choice/$', ChoiceAPIAdd.as_view(), name='choice_api_add'),
]
'''