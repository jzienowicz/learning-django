from rest_framework import serializers
from polls.models import Question, Choice


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = [
            'pk',
            'question_text',
            'user_id',
            'pub_date',
        ]
        read_only_fields = ['user_id']

    def update(self, instance, validated_data):
        instance.question_text = validated_data.get('question_text', instance.question_text)
        instance.save()


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = [
            'pk',
            'question',
            'choice_text',
            'votes',
            'user_id',
        ]
        read_only_fields = ['user_id', 'votes']

    def update(self, instance, validated_data):
        instance.choice_text = validated_data.get('choice_text', instance.choice_text)
        instance.save()


