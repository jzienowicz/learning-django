import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.conf import settings


class Question(models.Model):
    """
    model for questions
    """
    question_text = models.CharField(max_length=200)
    user_id = models.ForeignKey(get_user_model(),
                                blank=True,
                                null=True,
                                on_delete=models.SET_NULL)
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=settings.QUESTION_RECENTLY_THRESHOLD) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'


class Choice(models.Model):
    """
    model for choice for questions
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    user_id = models.ForeignKey(get_user_model(),
                                blank=True,
                                null=True,
                                on_delete=models.SET_NULL)

    def __str__(self):
        return self.choice_text


class VotedOn(models.Model):
    """
    many to many relationship between user and question
    has data if user voted for choice in question
    """
    user_id = models.ForeignKey(get_user_model(),
                                blank=True,
                                null=True,
                                on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
