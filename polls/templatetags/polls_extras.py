from django import template
from ..models import Choice, Question, VotedOn

register = template.Library()


@register.filter()
def questioninclude(value, arg):
    q = value.filter(user_id=arg)
    return q if q else False


@register.filter()
def questiondontinclude(value, arg):
    return value.exclude(user_id=arg)


@register.filter()
def alreadyvoted(value, arg):
    return False if VotedOn.objects.filter(user_id=value, question=arg) else True
