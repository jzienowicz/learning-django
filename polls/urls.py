from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('login/', views.LoginUser.as_view(), name='login'),
    path('logout/', views.LogoutUser.as_view(), name='logout'),
    path('add_question/', views.AddQuestion.as_view(), name='add_question'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/question_manipulation/', views.QuestionManipulation.as_view(), name='question_manipulation'),
    path('<int:question_id>/delete_question/', views.DeleteQuestion.as_view(), name='delete_question'),
    path('<int:question_id>/add_choice/', views.AddChoice.as_view(), name='add_choice'),
]