from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic, View
from django.utils import timezone
from django.contrib.auth import login, logout, authenticate
from django.shortcuts import get_object_or_404, render, redirect
from .forms import RegisterForm
from .models import Choice, Question, VotedOn
import sklearn
from django.db import models


class IndexView(generic.ListView):
    """
    View showing main page (index.html)
    """
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.all()



class DetailView(generic.DetailView):
    """
    View showing one question + its answers
    """
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    """
    View shown after voting for answer
    """
    model = Question
    template_name = 'polls/results.html'


class QuestionManipulation(View):
    """
    function used for managing voting by user
    it increase vote number of answer and record that user voted in that question
    and deletion of choices from question

    Parameters:
        id of question
    """
    def post(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "You didn't select a choice.",
            })
        else:
            if '_delete' in request.POST:
                selected_choice.delete()
                return HttpResponseRedirect(reverse('polls:detail', args=(question_id,)))
            selected_choice.votes += 1
            selected_choice.save()
            if request.user.is_authenticated:
                voted_on = VotedOn(user_id=request.user, question=question)
                voted_on.save()
            return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


class RegisterView(View):
    """
    function for registering new users
    """
    template_name = "polls/register.html"
    form = RegisterForm()

    def get(self, request):
        return render(request, self.template_name, {
            "form": self.form
        })

    def post(self, request):
        temp_form = RegisterForm(request.POST)
        if temp_form.is_valid():
            temp_form.save()
            return redirect("/polls")
        else:
            return render(request, self.template_name, {
                "form": self.form
            })


class LoginUser(View):
    """
    function for login
    """
    template_name = "polls/index.html"

    def get(self, request):
        return render(request, self.template_name, {
            "error_text": "Very weird error"
        })

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return render(request, self.template_name, {
                "error_text": "Incorrect user name or password"
            })


class LogoutUser(View):
    """
    function for logout
    """
    def post(self, request):
        logout(request)
        return redirect(request.META.get('HTTP_REFERER', '/'))


class AddQuestion(View):
    """
    function managing adding questions to database
    """
    template_name = "polls/add_question.html"

    def get(self, request):
        return render(request, self.template_name, {
            "success": False
        })

    def post(self, request):
        success = request.POST.get('question')
        if request.user.is_authenticated:
            question = Question(question_text=success, user_id=request.user, pub_date=timezone.now())
            question.save()
        else:
            question = Question(question_text=success, pub_date=timezone.now())
            question.save()
        return render(request, self.template_name, {
            "success": success
        })


class AddChoice(View):
    """
    function managing adding answer to question
    """
    def post(self, request, question_id):
        if request.user.is_authenticated:
            choice = Choice(choice_text=request.POST.get("choice"), question=Question.objects.get(pk=question_id), user_id=request.user)
            choice.save()
        else:
            choice = Choice(choice_text=request.POST.get("choice"), question=Question.objects.get(pk=question_id))
            choice.save()
        return HttpResponseRedirect(reverse('polls:detail', args=(question_id,)))


class DeleteQuestion(View):
    """
    delete question
    """
    def post(self, question_id):
        Question.objects.get(pk=question_id).delete()
        return HttpResponseRedirect(reverse('polls:index', args=()))
